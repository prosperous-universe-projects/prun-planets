# Prosperous Universe Planets API

An API to provide info on Prosperous Universe Planets written in Golang.

## Usage

The fastest way to get up and running to test the API along with the backing database is to run the included Docker Compose file.

The included docker-compose.yaml file will run a Postgres database container, build and run the Go API container and run an Adminer container for a quick and easy way to view/manange the database.

A local Go install should not be required to run the containers but Docker and Docker Compose are required along with an Internet connection to download containers and Go packages.

1. Clone this git repo
2. Run ```docker-compose up```, add the --build flag to rebuild the API containr if you have made changes. The API will create the "planets" table if it doesn't exist already.
3. Test the ping endpoint ```GET http://localhost:3000/api/v1/ping```
4. Add a planets data. The "data" field in the body json is stored and returned as straight json in Postgres. This allows the storage of all 6000+ lines of "plots" data which has been removed in this example.
      ```
      POST http://localhost:3000/api/v1/planets/new
      ```

      Body:
      ```json
      {
        "id": "3959d8b208acfb9a7bcb9aa418685e76",
        "naturalid": "XK-632a",
        "name": "Gibson",
        "data": {
          "gravity": 1.045715,
          "magneticField": 0.53771764,
          "mass": 6.104145347820257e+24,
          "massEarth": 1.0221274862391587,
          "orbit": {
            "semiMajorAxis": 25337876000,
            "eccentricity": 0.04596632331663093,
            "inclination": 0.0235419926385157,
            "rightAscension": 0,
            "periapsis": 0
          },
          "orbitIndex": 0,
          "pressure": 0.98157144,
          "radiation": 1.4261866e-24,
          "radius": 6302798,
          "resources": [
            {
              "materialId": "6e16dbf050b98d9c4fc9c615b3367a0f",
              "type": "GASEOUS",
              "factor": 0.16
            },
            {
              "materialId": "ec8dbb1d3f51d89c61b6f58fdd64a7f0",
              "type": "LIQUID",
              "factor": 0.19
            },
            {
              "materialId": "b9640b0d66e7d0ca7e4d3132711c97fc",
              "type": "MINERAL",
              "factor": 0.18
            },
            {
              "materialId": "c04de28c3e717f236d6ed177b89d9523",
              "type": "GASEOUS",
              "factor": 0.07
            }
          ],
          "sunlight": 94.42097,
          "surface": true,
          "temperature": 3.978007,
          "fertility": -0.24000001
        }
      }
      ```
  1. Get a single planet info ```GET http://localhost:3000/api/v1/planets/3959d8b208acfb9a7bcb9aa418685e76```
  2. Get info for all planets ```GET http://localhost:3000/api/v1/planets```
  3. Delete data for a planet ```DELETE http://localhost:3000/api/v1/planets/3959d8b208acfb9a7bcb9aa418685e76```
    

## Endpoints:

### Done:

- Ping test - GET /api/v1/ping
- Single Planet - GET /api/v1/planets/{{ naturalId }}
- All Planets - GET /api/v1/planets
- Add Planet - POST /api/v1/planets/new
- Remove Planet - DELETE /api/v1/planets/delete/{{ naturalId }}

### In-progress:

- swagger spec /api/v1/planets/swagger
- Update Existing Planet /api/v1/planets/update/{{ naturalId }}
- Search for Planet /api/v1/planets/search?resource={{ resourceCode }}
- Manage/Moderate adds/updates/deletes /api/v1/planets/manage/notifications

## To Do:

- [ ] Import multiple entries at once
- [ ] Data validation (human)
- [ ] Manage Entries (adds/updates/deletes /api/v1/planets/manage/notifications)
- [ ] Update entry
- [ ] Redis
- [ ] Swagger
- [x] Docker Compose
- [ ] Basic search/limiting results