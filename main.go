package main

import (
	"flag"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx"
)

var (
	connectionString = flag.String("conn", getenvWithDefault("DATABASE_URL", ""), "PostgreSQL connection string")
	listenAddr       = flag.String("addr", getenvWithDefault("LISTENADDR", ":3000"), "HTTP address to listen on")
)

// App is a representation of the application
type App struct {
	db     *sqlx.DB
	Router *mux.Router
	Logger *log.Logger
}

const tableCreationQuery = `create table if not exists planets
(
	planetId        text,
	naturalId       text,
	name            text,
	namer           json,
	namingDate      json,
	nameable        bool,
	celestialBodies json,
	id              text,
	country         json,
	governor        json,
	governingEntity json,
	currency        json,
	metaData        json,
	address         json,
	data            json,
	buildOptions    json,
	projects        json,
	localRules      json,
	constraint planets_pkey primary key (id)
)`

func (a *App) connectDatabase(connectionString string) {

	var err error

	// Postrgres Connection

	if connectionString == "" {
		log.Fatalln("Please pass the connection string using the -conn option")
	}

	a.db, err = sqlx.Connect("pgx", connectionString)
	if err != nil {
		log.Fatalf("Unable to establish connection: %v\n", err)
	}
}

func getenvWithDefault(name, defaultValue string) string {
	val := os.Getenv(name)
	if val == "" {
		val = defaultValue
	}

	return val
}

func (a *App) ensureTableExists() {
	if _, err := a.db.Exec(tableCreationQuery); err != nil {
		log.Fatal(err)
	}
}

func main() {

	flag.Parse()

	a := App{}
	a.connectDatabase(*connectionString)
	a.ensureTableExists()

	r := mux.NewRouter()

	r.HandleFunc("/api/v1/planetdata/ping", a.ping).Methods("GET")
	r.HandleFunc("/api/v1/planetdata/new", a.newPlanet).Methods("POST")
	r.HandleFunc("/api/v1/planetdata/all", a.getAllPlanets).Methods("GET")
	r.HandleFunc("/api/v1/planetdata/{id}", a.getPlanet).Methods("GET")
	r.HandleFunc("/api/v1/planetdata/{id}", a.updatePlanet).Methods("PUT")
	r.HandleFunc("/api/v1/planetdata/{id}", a.deletePlanet).Methods("DELETE")

	log.Printf("listening on %s\n", *listenAddr)
	log.Fatal(http.ListenAndServe(*listenAddr, r))
}
