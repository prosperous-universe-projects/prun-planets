package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	// "github.com/go-ozzo/ozzo-validation"
	// "github.com/go-ozzo/ozzo-validation/is"
	"github.com/gorilla/mux"
)

// send a payload of JSON content
func (a *App) respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}

// send a JSON error message
func (a *App) respondWithError(w http.ResponseWriter, code int, message string) {
	a.respondWithJSON(w, code, map[string]string{"error": message})

	log.Printf("App error: code %d, message %s", code, message)
}

func (a *App) ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Pong")
}

func (a *App) newPlanet(w http.ResponseWriter, r *http.Request) {
	var p Planet
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&p); err != nil {
		msg := fmt.Sprintf("Invalid request payload. Error: %s", err.Error())
		a.respondWithError(w, http.StatusBadRequest, msg)
		return
	}

	defer r.Body.Close()

	_, err := a.db.NamedExec(createPlanetQuery, &p)
	if err != nil {
		a.respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	a.respondWithJSON(w, http.StatusCreated, p)
}

func (a *App) getAllPlanets(w http.ResponseWriter, r *http.Request) {

	var p []Planet

	err := a.db.Select(&p, getPlanetsQuery)

	if err != nil {
		log.Println("Getting planets from the database failed")
		switch err {
		case sql.ErrNoRows:
			msg := fmt.Sprintf("Planets not found. Error: %s", err.Error())
			a.respondWithError(w, http.StatusNotFound, msg)
		default:
			a.respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}

	a.respondWithJSON(w, http.StatusOK, p)
}

func (a *App) getPlanet(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	log.Println(id)

	var p Planet
	err := a.db.Get(&p, getPlanetQuery, id)
	if err != nil {
		log.Println("Getting planet from the database failed")
		switch err {
		case sql.ErrNoRows:
			msg := fmt.Sprintf("Planet not found. Error: %s", err.Error())
			a.respondWithError(w, http.StatusNotFound, msg)
		default:
			a.respondWithError(w, http.StatusInternalServerError, err.Error())
		}
		return
	}

	a.respondWithJSON(w, http.StatusOK, p)
}

func (a *App) updatePlanet(w http.ResponseWriter, r *http.Request) {
	var p Planet
	decoder := json.NewDecoder(r.Body)
	if err := decoder.Decode(&p); err != nil {
		msg := fmt.Sprintf("Invalid request payload. Error: %s", err.Error())
		a.respondWithError(w, http.StatusBadRequest, msg)
		return
	}

	defer r.Body.Close()

	_, err := a.db.NamedExec(updatePlanetQuery, &p)
	if err != nil {
		a.respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	a.respondWithJSON(w, http.StatusCreated, p)
}

func (a *App) deletePlanet(w http.ResponseWriter, r *http.Request) {
	var p Planet
	vars := mux.Vars(r)
	id := vars["id"]

	_, err := a.db.Exec(deletePlanetQuery, id)

	if err != nil {
		a.respondWithError(w, http.StatusInternalServerError, err.Error())
	}

	a.respondWithJSON(w, http.StatusOK, p)
}
