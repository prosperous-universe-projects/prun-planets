package main

import (
	_ "github.com/jackc/pgx/stdlib"
	"github.com/jmoiron/sqlx/types"

	//"log"
	"time"
)

// Planet is a representation of the Planet data from the database
type Planet struct {
	PlanetID        string         `json:"planetId" db:"planetid"`
	NaturalID       string         `json:"naturalId" db:"naturalid"`
	Name            string         `json:"name" db:"name"`
	Namer           types.JSONText `json:"namer" db:"namer"`
	NamingDate      types.JSONText `json:"namingDate" db:"namingdate"`
	Nameable        bool           `json:"nameable" db:"nameable"`
	CelestialBodies types.JSONText `json:"celestialbodies" db:"celestialbodies"`
	ID              string         `json:"id" db:"id"`
	Country         types.JSONText `json:"country" db:"country"`
	Governor        types.JSONText `json:"governor" db:"governor"`
	GoverningEntity types.JSONText `json:"governingEntity" db:"governingentity"`
	Currency        types.JSONText `json:"currency" db:"currency"`
	MetaData        *EntryMetadata `json:"metadata" db:"metadata"`
	Address         types.JSONText `json:"address" db:"address"`
	Data            types.JSONText `json:"data" db:"data"`
	BuildOptions    types.JSONText `json:"buildoptions" db:"buildoptions"`
	Projects        types.JSONText `json:"projects" db:"projects"`
	LocalRules      types.JSONText `json:"localrules" db:"localrules"`
}

// EntryMetadata is related to the database entry itself, when was it last updated, who updated it, is it complete.
type EntryMetadata struct {
	Updated   time.Time `json:"updated"`
	Updater   string    `json:"updater"`
	Complete  bool      `json:"complete"`
	Validated bool      `json:"validated"`
	Validator string    `json:"validator"`
}

const (
	getPlanetsQuery   = "select * from planets"
	getPlanetQuery    = "select * from planets where id=$1 LIMIT 1"
	createPlanetQuery = `insert into planets (planetId, naturalId, name, namer, namingDate, nameable, celestialBodies,
											id, country, governor, governingEntity, currency, metadata, address, data, buildoptions, projects,
											localrules) values (:planetid, :naturalid, :name, :namer, :namingdate, :nameable, :celestialbodies,
											:id, :country, :governor, :governingentity, :currency, :metadata, :address, :data, :buildoptions, :projects,
											:localrules) returning id`
	updatePlanetQuery = `update planets SET planetId=:planetid, naturalId=:naturalid, name=:name, namer=:namer, namingDate=:namingdate, nameable=:nameable, celestialBodies=:celestialbodies,
											id=:id, country=:country, governor=:governor, governingEntity=:governingentity, currency=:currency, metadata=:metadata, address=:address, data=:data, buildoptions=:buildoptions, projects=:projects,
											localrules=:localrules where id=:id`
	deletePlanetQuery = "delete from planets where id=$1"
)
